package net.sf.okapi.tkit.integration;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import net.sf.okapi.common.ClassUtil;
import net.sf.okapi.common.FileUtil;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.XMLFileCompare;
import net.sf.okapi.common.integration.IntegrationtestUtils;
import net.sf.okapi.common.integration.RoundTripUtils;
import net.sf.okapi.filters.ts.TsFilter;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripTsTkitsIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	private TsFilter filter;
	
	@Rule
	public ErrorCollector errCol = new ErrorCollector();

	@Before
	public void setUp() throws Exception {
		filter = new TsFilter();
	}

	@After
	public void tearDown() throws Exception {
		filter.close();
	}

	@Test
	public void tsFiles() throws FileNotFoundException, URISyntaxException {		
		// run top level files (without config)
		for (File file : IntegrationtestUtils.getTestFiles("/ts/TS.dtd", Arrays.asList(".ts"))) {
			runTest(false, file, "okf_ts", null);
			runTest(true, file, "okf_ts", null);
		}

		// run each subdirectory where we assume there is a custom config)
		for(File d : IntegrationtestUtils.getSubDirs(ClassUtil.getResourceParent(IntegrationtestUtils.class, "/ts/TS.dtd")))
		{
			for(File c : IntegrationtestUtils.getConfigFile(d.getPath()))
			{
				for(File file : IntegrationtestUtils.getTestFiles(d.getPath(), Arrays.asList(".ts"), true))
				{					
					String configName = Util.getFilename(c.getAbsolutePath(), false);
					String customConfigPath = c.getParent();
					runTest(false, file, configName, customConfigPath);
					runTest(true, file, configName, customConfigPath);
				}
			}
		}
	}

	private void runTest(boolean segment, File file, String configName, String customConfigPath)
			throws FileNotFoundException, URISyntaxException {
		String f = file.getName();
		LOGGER.info(f);
		String root = file.getParent() + File.separator;
		String xliff = root + f + ".xliff";
		String original = root + f;
		String tkitMerged = root + f + ".tkitMerged";
		String merged = root + f + ".merged";
		List<String> locales = FileUtil.guessLanguages(file.getAbsolutePath());		
		LocaleId source = LocaleId.ENGLISH;
		if (locales.size() >= 1) {
			source = LocaleId.fromString(locales.get(0));
		}		
		LocaleId target = LocaleId.EMPTY;
		if (locales.size() >= 2) {
			target = LocaleId.fromString(locales.get(1));
		}
		
		RoundTripUtils.extract(source, target, original, xliff, configName, customConfigPath, segment);		
		RoundTripUtils.merge(source, target, false, original, xliff, tkitMerged, configName, customConfigPath);
		
		XMLFileCompare compare = new XMLFileCompare();				
		try {
			assertTrue("Compare Lines: " + f, compare.compareFilesPerLines(tkitMerged, tkitMerged));
		} catch(Throwable e) {
			errCol.addError(e);
		}
	}
}
