package net.sf.okapi.tkit.integration;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import net.sf.okapi.common.ClassUtil;
import net.sf.okapi.common.FileCompare;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.integration.IntegrationtestUtils;
import net.sf.okapi.common.integration.RoundTripUtils;
import net.sf.okapi.filters.regex.RegexFilter;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripRegexTkitsIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	private RegexFilter regexFilter;
	
	@Rule
	public ErrorCollector errCol = new ErrorCollector();
	
	@Before
	public void setUp() throws Exception {
		regexFilter = new RegexFilter();
	}

	@After
	public void tearDown() throws Exception {
		regexFilter.close();
	}

	@Test
	public void regexFiles() throws FileNotFoundException, URISyntaxException {		
		// run top level files (without config)
		for (File file : IntegrationtestUtils.getTestFiles("/regex/dummy.foo", Arrays.asList(".txt", ".regex"))) {
			runTest(false, file, "okf_regex", null);
			runTest(true, file, "okf_regex", null);
		}

		// run each subdirectory where we assume there is a custom config)
		for(File d : IntegrationtestUtils.getSubDirs(ClassUtil.getResourceParent(IntegrationtestUtils.class, "/regex/dummy.foo")))
		{
			for(File c : IntegrationtestUtils.getConfigFile(d.getPath()))
			{
				for(File file : IntegrationtestUtils.getTestFiles(d.getPath(), Arrays.asList(".txt", ".regex"), true))
				{					
					String configName = Util.getFilename(c.getAbsolutePath(), false);
					String customConfigPath = c.getParent();
					runTest(false, file, configName, customConfigPath);
					runTest(true, file, configName, customConfigPath);
				}
			}
		}
	}

	private void runTest(boolean segment, File file, String configName, String customConfigPath)
			throws FileNotFoundException, URISyntaxException {
		String f = file.getName();
		LOGGER.info(f);
		String root = file.getParent() + File.separator;
		String xliff = root + f + ".xliff";
		String original = root + f;
		String tkitMerged = root + f + ".tkitMerged";
		String merged = root + f + ".merged";
		
		RoundTripUtils.extract(LocaleId.ENGLISH, LocaleId.FRENCH, original, xliff, configName, customConfigPath, segment);		
		RoundTripUtils.merge(LocaleId.ENGLISH, LocaleId.FRENCH, false, original, xliff, tkitMerged, configName, customConfigPath);
		
		FileCompare compare = new FileCompare();		
		try {
			assertTrue("Compare Lines: " + f, compare.compareFilesPerLines(tkitMerged, tkitMerged, StandardCharsets.UTF_8.name()));
		} catch(Throwable e) {
			errCol.addError(e);
		}
	}
}
