package net.sf.okapi.xliffcompare.integration;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import net.sf.okapi.common.ClassUtil;
import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.integration.IntegrationtestUtils;
import net.sf.okapi.common.integration.RoundTripUtils;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.wiki.WikiFilter;
import net.sf.okapi.filters.xliff.XLIFFFilter;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WikXliffCompareiIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	private WikiFilter filter;
	
	@Rule
	public ErrorCollector errCol = new ErrorCollector();
	
	@Before
	public void setUp() throws Exception {
		filter = new WikiFilter();		
	}

	@After
	public void tearDown() throws Exception {
		filter.close();
	}
	
	@Test
	public void wikiXliffCompareFiles() throws FileNotFoundException, URISyntaxException {		
		// run top level files (without config)
		for (File file : IntegrationtestUtils.getTestFilesNoRecurse("/wikitext/simple.wiki", Arrays.asList(".wiki", ".txt"))) {
			runTest(true, file, "okf_wiki", null, null);
		}

		// run each subdirectory where we assume there is a custom config)
		for(File d : IntegrationtestUtils.getSubDirs(ClassUtil.getResourceParent(IntegrationtestUtils.class, "/wikitext/simple.wiki")))
		{
			for(File c : IntegrationtestUtils.getConfigFile(d.getPath()))
			{
				for(File file : IntegrationtestUtils.getTestFiles(d.getPath(), Arrays.asList(".wiki", ".txt"), true))
				{					
					String configName = Util.getFilename(c.getAbsolutePath(), false);
					String customConfigPath = c.getParent();
					runTest(true, file, configName, customConfigPath, d);
				}
			}
		}
	}

	private void runTest(boolean segment, File file, String configName, String customConfigPath, File subDir)
			throws FileNotFoundException, URISyntaxException {
		String f = file.getName();
		LOGGER.info(f);
		String root = file.getParent() + File.separator;
		String xliff = root + f + ".xliff";
		String original = root + f;
		String sd = ((subDir == null) ? "" : subDir.getName() + "/");
		String xliffPrevious = XliffCompareUtils.CURRENT_XLIFF_ROOT + "wikitext/" +  sd + f + ".xliff"; 
		
		RoundTripUtils.extract(LocaleId.ENGLISH, LocaleId.FRENCH, original, xliff, configName, customConfigPath, segment);		
		
		XLIFFFilter xf = new XLIFFFilter();
		RawDocument ox = new RawDocument(Util.toURI(xliff), StandardCharsets.UTF_8.name(), LocaleId.ENGLISH, LocaleId.FRENCH);
		List<Event> oe = IntegrationtestUtils.getTextUnitEvents(xf, ox);
		
		RawDocument px = new RawDocument(Util.toURI(xliffPrevious), StandardCharsets.UTF_8.name(), LocaleId.ENGLISH, LocaleId.FRENCH);
		List<Event> pe = IntegrationtestUtils.getTextUnitEvents(xf, px);
		try {
			assertTrue("Compare Lines: " + f, FilterTestDriver.compareEvents(oe, pe));
		} catch(Throwable e) {
			errCol.addError(e);
		} finally {
			ox.close();
			px.close();
		}
	}
}
	
   
