package net.sf.okapi.xliffcompare.integration;

import java.io.File;

public final class XliffCompareUtils {
	public static final String CURRENT_XLIFF_ROOT = new File(XliffCompareUtils.class.getResource("/XLIFF_M28_05_12_2015/dummy.txt").getPath()).getParent() + File.separator;
}
