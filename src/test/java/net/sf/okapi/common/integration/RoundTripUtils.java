package net.sf.okapi.common.integration;

import java.io.File;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.DefaultFilters;
import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.filterwriter.XLIFFWriter;
import net.sf.okapi.common.filterwriter.XLIFFWriterParameters;
import net.sf.okapi.common.pipelinedriver.BatchItemContext;
import net.sf.okapi.common.pipelinedriver.IPipelineDriver;
import net.sf.okapi.common.pipelinedriver.PipelineDriver;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.lib.tkit.step.LegacyXliffMergerStep;
import net.sf.okapi.lib.tkit.step.OriginalDocumentXliffMergerStep;
import net.sf.okapi.steps.common.FilterEventsWriterStep;
import net.sf.okapi.steps.common.RawDocumentToFilterEventsStep;
import net.sf.okapi.steps.common.RawDocumentWriterStep;
import net.sf.okapi.steps.common.codesimplifier.PostSegmentationCodeSimplifierStep;
import net.sf.okapi.steps.segmentation.Parameters;
import net.sf.okapi.steps.segmentation.SegmentationStep;

public final class RoundTripUtils {
	
	public static void extract(LocaleId english, LocaleId french, String original, String xliff,
			String configName, String customConfigPath, boolean segment) throws URISyntaxException {
		extract(english, french, original, xliff, configName, customConfigPath, segment, false);
	}
	
    public static void extract(LocaleId source, LocaleId target, String originalPath, String outputPath, String filterConfig, String customConfigPath, boolean segment, boolean simplify) throws URISyntaxException {
    	 FilterConfigurationMapper mapper = new FilterConfigurationMapper();
         DefaultFilters.setMappings(mapper, false, true);
         if (customConfigPath != null) {
             mapper.setCustomConfigurationsDirectory(customConfigPath);
             mapper.addCustomConfiguration(filterConfig);
             // look for secondary filter config - only one per subdir
             File secondary = IntegrationtestUtils.getSecondaryConfigFile(customConfigPath, filterConfig);
             if (secondary != null) {
             	mapper.addCustomConfiguration(Util.getFilename(secondary.getName(), false));
             }         
             mapper.updateCustomConfigurations();
         }
         
		// Create the driver
		PipelineDriver driver = new PipelineDriver();
		driver.setFilterConfigurationMapper(mapper);

		// Raw document to filter events step 
		RawDocumentToFilterEventsStep rd2feStep = new RawDocumentToFilterEventsStep();
		driver.addStep(rd2feStep);
		
		if (segment) {
			SegmentationStep ss = new SegmentationStep();
			ss.setSourceLocale(source);
			List<LocaleId> tl = new LinkedList<>();
			tl.add(target);
			ss.setTargetLocales(tl);
			Parameters params = (Parameters)ss.getParameters();
			params.setSegmentSource(true);
			params.setSegmentTarget(true);
			params.setSourceSrxPath(RoundTripUtils.class.getClassLoader().getResource("default.srx").getPath());
			params.setTargetSrxPath(RoundTripUtils.class.getClassLoader().getResource("default.srx").getPath());
			params.setCopySource(false);
			driver.addStep(ss);
		}
		
		if (simplify) {
			driver.addStep(new PostSegmentationCodeSimplifierStep());
		}
				
		// Filter events to raw document final step (using the XLIFF writer)
		FilterEventsWriterStep fewStep = new FilterEventsWriterStep();
		
		XLIFFWriter writer = new XLIFFWriter();
		fewStep.setFilterWriter(writer);
		XLIFFWriterParameters params = (XLIFFWriterParameters)writer.getParameters();		
		params.setPlaceholderMode(false);
        params.setIncludeAltTrans(true);
        params.setEscapeGt(true);
        params.setIncludeCodeAttrs(true);
        params.setCopySource(true);
        params.setIncludeIts(true);
        params.setIncludeNoTranslate(true);
        params.setToolId("okapi");
        params.setToolName("okapi-tests");
        params.setToolCompany("okapi");
        params.setToolVersion("M29");
		
		fewStep.setDocumentRoots(Util.getDirectoryName(originalPath));
		driver.addStep(fewStep);

		RawDocument originalDoc = new RawDocument(Util.toURI(originalPath), StandardCharsets.UTF_8.name(), source, target);
        originalDoc.setFilterConfigId(filterConfig);
		
		driver.addBatchItem(originalDoc, new File(outputPath).toURI(), StandardCharsets.UTF_8.name());

		// Process
		driver.processBatch();
	}

    public static void merge(LocaleId source, LocaleId target, Boolean legacy, String originalPath, String xlfPath, String outputPath, String filterConfig, String customConfigPath) throws URISyntaxException {
        FilterConfigurationMapper mapper = new FilterConfigurationMapper();
        DefaultFilters.setMappings(mapper, false, true);
        if (customConfigPath != null) {
            mapper.setCustomConfigurationsDirectory(customConfigPath);
            mapper.addCustomConfiguration(filterConfig);
            // look for secondary filter config - only one per subdir
            File secondary = IntegrationtestUtils.getSecondaryConfigFile(customConfigPath, filterConfig);
            if (secondary != null) {            	
            	mapper.addCustomConfiguration(Util.getFilename(secondary.getName(), false));
            }            
            mapper.updateCustomConfigurations();
        }
        
        RawDocument originalDoc = new RawDocument(Util.toURI(originalPath), StandardCharsets.UTF_8.name(), source, target);
        originalDoc.setFilterConfigId(filterConfig);
        
        IPipelineDriver driver = new PipelineDriver();  
        driver.setFilterConfigurationMapper(mapper);
        BatchItemContext bic = new BatchItemContext(
                new RawDocument(Util.toURI(xlfPath), StandardCharsets.UTF_8.name(), source, target), 
                Util.toURI(outputPath), 
                StandardCharsets.UTF_8.name(), 
                originalDoc);
        driver.addBatchItem(bic);
        if (legacy) {
            driver.addStep(new LegacyXliffMergerStep());
        } else {
            driver.addStep(new OriginalDocumentXliffMergerStep());
        }
        driver.addStep(new RawDocumentWriterStep());
        driver.processBatch();
        driver.destroy();       
    }
    
    /**
     * Assumes merge has been called to set source and target locales
     */
    public static boolean compareOriginalWithTarget(IFilter filter, String originalfile, String targetFile) {
    	return compareOriginalWithTarget(filter, originalfile, targetFile, true, false, false, false);
    }
    
    /**
     * Assumes merge has been called to set source and target locales
     */
    public static boolean compareOriginalWithTarget(IFilter filter, String originalfile, String targetFile, 
    		boolean includeSkeleton, boolean ignoreSkelWhitespace, boolean ignoreSegmentation, boolean ignoreFragmentWhitespace) {
    	RawDocument ord = new RawDocument(Util.toURI(originalfile), "UTF-8", LocaleId.ENGLISH, LocaleId.FRENCH);
		RawDocument trd = new RawDocument(Util.toURI(targetFile), "UTF-8", LocaleId.ENGLISH, LocaleId.FRENCH);
		List<Event> o = IntegrationtestUtils.getEvents(filter, ord);
		List<Event> t = IntegrationtestUtils.getEvents(filter, trd);
		boolean r =  FilterTestDriver.compareEvents(o, t, includeSkeleton, ignoreSkelWhitespace, ignoreSegmentation, ignoreFragmentWhitespace);
		ord.close();
		trd.close();
		o.clear();
		t.clear();
		return r;
    }
}
