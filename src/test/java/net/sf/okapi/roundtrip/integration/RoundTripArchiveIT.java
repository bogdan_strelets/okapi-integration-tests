package net.sf.okapi.roundtrip.integration;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;

import net.sf.okapi.common.ClassUtil;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.MimeTypeMapper;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.ZipXMLFileCompare;
import net.sf.okapi.common.filters.FilterConfiguration;
import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.common.integration.IntegrationtestUtils;
import net.sf.okapi.common.integration.RoundTripUtils;
import net.sf.okapi.filters.archive.ArchiveFilter;
import net.sf.okapi.filters.archive.Parameters;
import net.sf.okapi.filters.tmx.TmxFilter;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripArchiveIT {
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	private ArchiveFilter archiveFilter;
	Parameters params;
	
	@Rule
	public ErrorCollector errCol = new ErrorCollector();
	
	@Before
	public void setUp() throws Exception {
		FilterConfigurationMapper fcm = new FilterConfigurationMapper();
		// Create configuration for tmx extension (if we need text units from
		// tmx as well)
		fcm.addConfiguration(new FilterConfiguration(
				"okf_tmx",
				MimeTypeMapper.TMX_MIME_TYPE,
				TmxFilter.class.getName(),
				// "net.sf.okapi.filters.tmx.TmxFilter",
				"TMX",
				"Configuration for Translation Memory eXchange (TMX) documents.",
				null, ".tmx;"));

		archiveFilter = new ArchiveFilter();
		archiveFilter.setFilterConfigurationMapper(fcm);

		params = new Parameters();
		params.setFileNames("*.xliff, *.tmx, *.xlf");
		params.setConfigIds("okf_xliff, okf_tmx, okf_xliff");
		archiveFilter.setParameters(params);
	}

	@After
	public void tearDown() throws Exception {
		archiveFilter.close();
	}
	
	@Test
	public void archiveFiles() throws FileNotFoundException, URISyntaxException {		
		// run top level files (without config)
		for (File file : IntegrationtestUtils.getTestFiles("/archive/test.archive", Arrays.asList(".archive", ".zip"))) {
			archiveFilter.setParameters(params);
			runTest(false, file, "okf_archive", null);
			runTest(true, file, "okf_archive", null);
		}
		
		// run each subdirectory where we assume there is a custom config)
		for(File d : IntegrationtestUtils.getSubDirs(ClassUtil.getResourceParent(IntegrationtestUtils.class, "/archive/test.archive")))
		{
			for(File c : IntegrationtestUtils.getConfigFile(d.getPath()))
			{
				for(File file : IntegrationtestUtils.getTestFiles(d.getPath(), Arrays.asList(".archive", ".zip"), true))
				{
					String configName = Util.getFilename(c.getAbsolutePath(), false);
					String customConfigPath = c.getParent();
					runTest(false, file, configName, customConfigPath);
					runTest(true, file, configName, customConfigPath);
				}
			}
		}
	}
	
	private void runTest(boolean segment, File file, String configName, String customConfigPath)
			throws FileNotFoundException, URISyntaxException {
		String f = file.getName();
		LOGGER.info(f);
		String root = file.getParent() + File.separator;
		String xliff = root + f + ".xliff";
		String original = root + f;
		String tkitMerged = root + f + ".tkitMerged";
		String mergedRoundTrip = root + f + ".mergedRoundTrip";
		
		RoundTripUtils.extract(LocaleId.ENGLISH, LocaleId.FRENCH, original, xliff, configName, customConfigPath, segment);		
		RoundTripUtils.merge(LocaleId.ENGLISH, LocaleId.FRENCH, false, original, xliff, tkitMerged, configName, customConfigPath);
		
		RoundTripUtils.extract(LocaleId.ENGLISH, LocaleId.FRENCH, tkitMerged, xliff, configName, customConfigPath, segment);		
		RoundTripUtils.merge(LocaleId.ENGLISH, LocaleId.FRENCH, false, tkitMerged, xliff, mergedRoundTrip, configName, customConfigPath);
		
		ZipXMLFileCompare compare = new ZipXMLFileCompare();		
		try {
			assertTrue("Compare Lines: " + f, compare.compareFiles(mergedRoundTrip, tkitMerged));
		} catch(Throwable e) {
			errCol.addError(e);
		}
	}
}
