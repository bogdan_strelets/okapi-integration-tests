package net.sf.okapi.roundtrip.integration;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.okapi.common.ClassUtil;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.integration.IntegrationtestUtils;
import net.sf.okapi.common.integration.RoundTripUtils;
import net.sf.okapi.filters.yaml.YamlFilter;

public class RoundTripYamlIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	private YamlFilter yamlFilter;
	
	@Rule
	public ErrorCollector errCol = new ErrorCollector();

	@Before
	public void setUp() throws Exception {
		yamlFilter = new YamlFilter();
	}

	@After
	public void tearDown() throws Exception {
		yamlFilter.close();
	}
	
	@Ignore("private test, but leave here for future use")
	public void yamlWithHtmlSubfilter() throws FileNotFoundException, URISyntaxException {		
		// run each subdirectory where we assume there is a custom config)
		for(File d : IntegrationtestUtils.getSubDirs(ClassUtil.getResourceParent(IntegrationtestUtils.class, "/yaml/subfilter/dummy.txt")))
		{
			for(File c : IntegrationtestUtils.getConfigFile(d.getPath()))
			{
				for(File file : IntegrationtestUtils.getTestFiles(d.getPath(), Arrays.asList(".yml", ".yaml"), true))
				{					
					String configName = Util.getFilename(c.getAbsolutePath(), false);
					String customConfigPath = c.getParent();
					runTest(true, file, configName, customConfigPath);
				}
			}
		}
	}
	
	@Ignore("private test, but leave here for future use")
	public void yamlSingleFile() throws FileNotFoundException, URISyntaxException {		
		// run top level files (without config)
		for (File file : IntegrationtestUtils.getTestFiles("/yaml/examples/ios_emoji_surrogate.yaml", Arrays.asList(".yaml"))) {
			runTest(true, file, "okf_yaml", null);
		}
	}
	
	@Test
	public void yamlFiles() throws FileNotFoundException, URISyntaxException {		
		// run top level files (without config)
		for (File file : IntegrationtestUtils.getTestFiles("/yaml/en.yml", Arrays.asList(".yml", ".yaml"))) {
			runTest(false, file, "okf_yaml", null);
			runTest(true, file, "okf_yaml", null);
		}

		// run each subdirectory where we assume there is a custom config)
		for(File d : IntegrationtestUtils.getSubDirs(ClassUtil.getResourceParent(IntegrationtestUtils.class, "/yaml/en.yml")))
		{
			for(File c : IntegrationtestUtils.getConfigFile(d.getPath()))
			{
				for(File file : IntegrationtestUtils.getTestFiles(d.getPath(), Arrays.asList(".yml", ".yaml"), true))
				{					
					String configName = Util.getFilename(c.getAbsolutePath(), false);
					String customConfigPath = c.getParent();
					runTest(false, file, configName, customConfigPath);
					runTest(true, file, configName, customConfigPath);
				}
			}
		}
	}

	private void runTest(boolean segment, File file, String configName, String customConfigPath)
			throws FileNotFoundException, URISyntaxException {
		String f = file.getName();
		LOGGER.info(f);
		String root = file.getParent() + File.separator;
		String xliff = root + f + ".xliff";
		String original = root + f;
		String tkitMerged = root + f + ".tkitMerged";
		String mergedRoundTrip = root + f + ".mergedRoundTrip";
		try {
			RoundTripUtils.extract(LocaleId.ENGLISH, LocaleId.FRENCH, original, xliff, configName, customConfigPath, segment);		
			RoundTripUtils.merge(LocaleId.ENGLISH, LocaleId.FRENCH, false, original, xliff, tkitMerged, configName, customConfigPath);
			
			RoundTripUtils.extract(LocaleId.ENGLISH, LocaleId.FRENCH, tkitMerged, xliff, configName, customConfigPath, segment);		
			RoundTripUtils.merge(LocaleId.ENGLISH, LocaleId.FRENCH, false, tkitMerged, xliff, mergedRoundTrip, configName, customConfigPath);
			
			// can't compare files as lines have been wrapped
			assertTrue("Compare original and merged: " + f,
					RoundTripUtils.compareOriginalWithTarget(yamlFilter, original, mergedRoundTrip, false, true, false, true));
			
		} catch(Throwable e) {
			System.err.println("ERROR+++: " + f);
			errCol.addError(e);
		}
	}
}
